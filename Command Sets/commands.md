# Command Sets

## Standard Command

### ON - COMMAND
- ON - Change state of device hardware to 1.
  ```
  ON DEVICE_NAME
  ```
- SW - Switch number of device.
  ```
  SW SWITCH_NUMBER
  ```
- AT - Run this command at specific time.
- TANK - Run this command when tank level is > value.
- REPEAT - Run this command again in XXX (m/h)
- HOLD - Hold operation in XXX (m/h)
- RAIN - Run this command when rain sensor is rained (1) or not (0)
- PUMP - Enable pump (Automatic check pupm status) during run command
```
Example:

$ ON Solenoid_A SW 1 AT 9:30 TANK Tank_A 3 REPEAT 12 h HOLD 10 m RAIN Rain_A 0 SOIL Soil_A 0 PUMP Pump_A 
```
